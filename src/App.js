import React from 'react'
import './App.css'
import countries from './countries.json'

class App extends React.Component {
  state = {
    inputValue: '',
    items: countries,
    filteredItems: [],
    timeout: null
  }

  componentDidMount() {
    this.setState({
      filteredItems: this.state.items
    })
  }

  filterAutoComplete = async (value) => {
    if(value === '') {
      this.setState({ filteredItems: this.state.items })
      return
    }

    await fetch(`https://restcountries.com/v3.1/name/${value}`)
      .then((response) => response.json())
      .then((json) => {
        if(json.status === 404) {
          this.setState({ filteredItems: [] })
          return
        }
        this.setState({ filteredItems: json })
      })
      .catch((e) => alert('Error ocurred'))
  }

  onClick = (name) => {
    this.setState({ inputValue: name.common })
  }

  onChange = async (e) => {
    const { value } = e.target

    clearTimeout(this.state.timeout)

    this.setState({
      inputValue: value,
      timeout: setTimeout(async () => {
        await this.filterAutoComplete(value)
      }, 300)
    })
  }

  renderFilterList = () => {
    const { filteredItems } = this.state

    if(filteredItems.length === 0) {
      return <p>No result found.</p>
    }

    return filteredItems.map(({ name }) => (
      <div
        className="item"
        key={name.common}
        onClick={e => this.onClick(name)}
      >
          { name.common }
      </div>
    ))
  }

  render() {
    return (
      <div className="App">
        <input
          autoFocus
          onChange={this.onChange}
          value={this.state.inputValue}
        />
        <div className="items">
          { this.renderFilterList() }
        </div>
      </div>
    )
  }
}

export default App;
