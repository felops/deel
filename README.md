# To run
Just install the dependencies and run npm start

`npm install && npm start`


# Questions

## 1. What is the difference between Component and PureComponent? give an example where it might break my app.
The difference is how deep they compare the props they receive. In the component it compare if the props changed, but in the PureComponent it will compare only primitive. If you receive an object, the Component will compare it, but the PureComponent will compare if the reference for this object changed (which will be different in every render case this object is created in every render).

## 2. Context + ShouldComponentUpdate might be dangerous. Can think of why is that?
I don't.. What I guess is because you have the value coming from your Context and if you use a object in the Context it will always render when the value on Context changes. It could trigger the ShouldComponentUpdate, that could trigger the re-render on the Context, starting a loop of re-rendering.

## 3. Describe 3 ways to pass information from a component to its PARENT.
You can pass information by some library management (Redux, RecoilJs etc), using the Context API, or receiving a function that give back the information to the parent.

## 4. Give 2 ways to prevent components from re-rendering.
One of them is to use memoization in order to avoid render components that do not need to render again. The other one could be only declouping the components. For example, if you have this structure below and Component1 always get new render but Component2 don't, you are re-rendering Component2 every time, because React renders the whole tree.
```html
<>
  <Component1>
    <Component2 />
  </Component1>
</>
```

But if you change to something like this:
```html
<>
  <Component1 />
  <Component2 />
</>
```
If Component1 changes only it will be re-rendered, as Component2 is not children from Component1 anymore - not trigering unnecessary re-render only to be in the tree that is being updated.

## 5. What is a fragment and why do we need it? Give an example where it might break my app.
All render in React should have only one component in the root. If you want to render more than 1 component, you have to wrap them with either with component or with a fragment (which is like an empty tag). Here is a example where a missing fragment could break the app:
```html
render () {
  return (
    <Component1 />
    <Component2 />
    <Component3 />
  )
}
```

## 6. Give 3 examples of the HOC pattern.
I can't think in 3, but I would say that the Redux library for class component is a good example of HOC.

## 7. what's the difference in handling exceptions in promises, callbacks and async...await.
To handle exceptions in the promises you mostly likely to use the chain ".catch". To handle exceptions in async/await you are required to use the try/catch. In the calbacks I am not sure, but I would say that the callback itself need to handle the exceptions.

## 8. How many arguments does setState take and why is it async.
setState receive 2 arguments, the first one is the object to mutate and the second one is the callback when the operation finishes. Also, it is async because react batch all operation in a interval of time to update the DOM in one shot. Generally when you set the state you need to update the DOM - which is expensive. To avoid too much operations, React try to optimize it batching renders operations.

## 9. List the steps needed to migrate a Class to Function Component.
First of all you need to change the declaration from the component from class to function. After that, the next step would be get rid of the all states from "this.state..." to the hook setState. After that, basically you need to change the methods to being const receiving functions and get rid of the others "this..." that are in the code. If you use redux, you also need to remove the mapStateToProps thing and use the useSelector hooks to get values from stoe.

## 10. List a few ways styles can be used with components.
You can use the style tag passing a object with the style, you can use css class as I used in this one. But you can also use some library, as the styled-components, where you basically can declare a style with the element and use it across the app.

## 11. How to render an HTML string coming from the server.
You have a function in react that allows you to render the code directly from the server. I do not remember off the top of my head, but it is a function that renders to you. It is "_dangeroulyHTMLRender" or something like that. I know it because I have t use one time.